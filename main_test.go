package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/ory/dockertest"
)

func TestGlossaryWebfront(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Glossary webfront Suite")
}

var pool *dockertest.Pool
var res *dockertest.Resource

var _ = SynchronizedBeforeSuite(func() []byte {
	if !isCI() {
		pool, _ := dockertest.NewPool("")
		res, _ = pool.Run("postgres", "11.3-alpine", []string{"POSTGRES_PASSWORD=secret", "POSTGRES_DB=glossary"})
	}
	time.Sleep(10 * time.Second)
	if !isCI() {
		return []byte(res.GetPort("5432/tcp"))
	}
	return []byte("")

}, func(port []byte) {
	if !isCI() {
		os.Setenv("POSTGRES_HOST", "localhost")
		os.Setenv("POSTGRES_DATABASE", "glossary")
		os.Setenv("POSTGRES_PORT", string(port))
	}
	os.Setenv("POSTGRES_PASS", "unknown")
	err := initDB()
	Expect(err).NotTo(BeNil())

	os.Setenv("POSTGRES_PASS", "secret")
	err = initDB()
	Expect(err).To(BeNil())

	addHandler()
})

var _ = BeforeEach(func() {

})

var _ = SynchronizedAfterSuite(func() {
}, func() {
	if !isCI() {
		res.Close()
	}
})

var _ = Describe("Glossary webfront", func() {
	Describe("Setup should work", func() {
		BeforeEach(func() {
		})

		It("GetEnv Tests", func() {
			os.Unsetenv("ENV")
			Expect(GetEnv("ENV", "TEST_VAL")).To(BeEquivalentTo("TEST_VAL"))
			os.Setenv("ENV", "")
			Expect(GetEnv("ENV", "TEST_VAL")).To(BeEquivalentTo(""))
			os.Setenv("ENV", "TEST")
			Expect(GetEnv("ENV", "TEST_VAL")).To(BeEquivalentTo("TEST"))
			os.Unsetenv("ENV")

		})

		It("API Tests", func() {
			entry := GlossaryEntry{
				Term:        "Test",
				LongName:    "Testterm Longname",
				Translation: "(de) Testterm",
				Description: []string{"This", "is", "a", "Test"},
				Link:        "http://test.com",
				Tag:         []string{"test", "tag#1", "tag#2"},
				Further:     []string{"Another test entry"},
			}
			testHTTP("GET", "/api", []byte(""), apiHandler, &GlossaryEntry{}, &GlossaryEntry{})
			type returnValue struct {
				StatusCode int
				Message    string
			}
			testHTTP("POST", "/api", []byte(""), apiHandler, &returnValue{}, &returnValue{
				StatusCode: 500,
				Message:    "Could not decode JSON",
			})
			testHTTP("POST", "/api", []byte("{test"), apiHandler, &returnValue{}, &returnValue{
				StatusCode: 500,
				Message:    "Could not decode JSON",
			})

			jsonString, _ := json.Marshal(entry)
			testHTTP("POST", "/api", jsonString, apiHandler, &returnValue{}, &returnValue{
				StatusCode: 200,
				Message:    "Success",
			})
			// Sould not create second entry because of unique restrictions
			testHTTP("POST", "/api", jsonString, apiHandler, &returnValue{}, &returnValue{
				StatusCode: 500,
				Message:    "Could not create entry",
			})

			// Should update now
			entry2 := GlossaryEntry{
				ID:          1,
				Term:        "Test2",
				LongName:    "Testterm Longname2",
				Translation: "(de) Testterm2",
				Description: []string{"This", "is", "a", "Test"},
				Link:        "http://test.com",
				Tag:         []string{"test", "tag#1", "tag#2"},
				Further:     []string{"Another test entry"},
			}
			jsonString2, _ := json.Marshal(entry2)
			testHTTP("POST", "/api", jsonString2, apiHandler, &returnValue{}, &returnValue{
				StatusCode: 200,
				Message:    "Success",
			})

			testHTTP("DELETE", "/api", []byte("{test"), apiHandler, &returnValue{}, &returnValue{
				StatusCode: 500,
				Message:    "Could not decode JSON",
			})

			testHTTP("DELETE", "/api", jsonString2, apiHandler, &returnValue{}, &returnValue{
				StatusCode: 200,
				Message:    "Success",
			})

			testHTTP("DELETE", "/api", jsonString2, apiHandler, &returnValue{}, &returnValue{
				StatusCode: 500,
				Message:    "Could not delete entry",
			})

			entry2.ID = 400
			jsonString2, _ = json.Marshal(entry2)
			testHTTP("POST", "/api", jsonString2, apiHandler, &returnValue{}, &returnValue{
				StatusCode: 500,
				Message:    "Could not update entry",
			})
			testHTTP("GET", "/api", []byte(""), apiHandler, make(map[string]interface{}), make(map[string]interface{}))
			//testHTTP("POST", "/api", jsonString, apiHandler, 200)
			db.Close()
			testHTTP("GET", "/api", []byte(""), apiHandler, &returnValue{}, &returnValue{
				StatusCode: 500,
				Message:    "Could not fetch any entries",
			})

		})
		It("API Tests", func() {
			var req *http.Request
			var err error
			req, err = http.NewRequest("GET", "/", nil)
			Expect(err).To(BeNil())
			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(indexHandler)
			handler.ServeHTTP(rr, req)
			Expect(rr.Code).To(BeEquivalentTo(200))

			req, err = http.NewRequest("PATCH", "/api", bytes.NewBuffer([]byte(`{"test":"true"}`)))
			Expect(err).To(BeNil())
			rr = httptest.NewRecorder()
			handler = http.HandlerFunc(apiHandler)
			handler.ServeHTTP(rr, req)
			Expect(rr.Code).To(BeEquivalentTo(404))

		})
	})
})

func isCI() bool {
	if GetEnv("CI_JOB_ID", "") == "" {
		return false
	}
	return true
}

func testHTTP(method string, route string, payload []byte, handlerFunc func(http.ResponseWriter, *http.Request), model interface{}, expected interface{}) {
	var req *http.Request
	var err error
	switch method {
	case "POST":
		body := bytes.NewBuffer(payload)
		req, err = http.NewRequest(method, route, body)
	case "DELETE":
		body := bytes.NewBuffer(payload)
		req, err = http.NewRequest(method, route, body)
	case "GET":
		req, err = http.NewRequest(method, route, nil)
	}
	Expect(err).To(BeNil())
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(handlerFunc)
	handler.ServeHTTP(rr, req)
	b, err := ioutil.ReadAll(rr.Body)
	fmt.Println(string(b))
	Expect(err).To(BeNil())

	json.Unmarshal(b, model)

	Expect(model).To(BeEquivalentTo(expected))

}
