package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

var db *pg.DB

type GlossaryEntry struct {
	ID          int64
	Term        string `sql:",unique"`
	LongName    string
	Translation string
	Description []string
	Link        string
	Tag         []string
	Further     []string
}

type Return struct {
	StatusCode int
	Message    string
}

func GetEnv(key string, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func SaveMarshal(i interface{}) []byte {
	jsonStr, _ := json.Marshal(i)
	return jsonStr
}

func apiHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		body, _ := ioutil.ReadAll(r.Body)

		var entry GlossaryEntry
		err := json.Unmarshal(body, &entry)
		fmt.Println(entry)
		if err != nil {
			w.Write(SaveMarshal(Return{
				StatusCode: 500,
				Message:    "Could not decode JSON",
			}))
			return
		}
		if entry.ID == 0 {
			err = db.Insert(&entry)
			if err != nil {
				w.Write(SaveMarshal(Return{
					StatusCode: 500,
					Message:    "Could not create entry",
				}))
				return
			}
		} else {
			err := db.Update(&entry)
			if err != nil {
				w.Write(SaveMarshal(Return{
					StatusCode: 500,
					Message:    "Could not update entry",
				}))
				return
			}
		}
		w.Write(SaveMarshal(Return{
			StatusCode: 200,
			Message:    "Success",
		}))
	} else if r.Method == "DELETE" {
		body, _ := ioutil.ReadAll(r.Body)

		var entry GlossaryEntry
		err := json.Unmarshal(body, &entry)
		if err != nil {
			w.Write(SaveMarshal(Return{
				StatusCode: 500,
				Message:    "Could not decode JSON",
			}))
			return
		}
		err = db.Delete(&entry)
		if err != nil {
			w.Write(SaveMarshal(Return{
				StatusCode: 500,
				Message:    "Could not delete entry",
			}))
			return
		}
		w.Write(SaveMarshal(Return{
			StatusCode: 200,
			Message:    "Success",
		}))
	} else if r.Method == "GET" {
		var entries []GlossaryEntry
		err := db.Model(&entries).Order("id DESC").Select()
		if err != nil {
			w.Write(SaveMarshal(Return{
				StatusCode: 500,
				Message:    "Could not fetch any entries",
			}))
			return
		}
		jsonStr := SaveMarshal(entries)
		w.Write(jsonStr)
	} else {
		http.NotFound(w, r)
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "index", nil)
}

func renderTemplate(w http.ResponseWriter, tmpl string, p interface{}) {
	var templates = template.Must(template.ParseFiles(tmpl + ".html"))
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func initDB() error {
	postgresHost := GetEnv("POSTGRES_HOST", "0.0.0.0")
	postgresPort := GetEnv("POSTGRES_PORT", "5432")
	postgresUser := GetEnv("POSTGRES_USER", "postgres")
	postgresPass := GetEnv("POSTGRES_PASS", "secret")
	postgresDb := GetEnv("POSTGRES_DATABASE", "postgres")
	db = pg.Connect(&pg.Options{
		Addr:     fmt.Sprintf("%s:%s", postgresHost, postgresPort),
		User:     postgresUser,
		Password: postgresPass,
		Database: postgresDb,
	})
	err := db.CreateTable(&GlossaryEntry{}, &orm.CreateTableOptions{
		IfNotExists: true,
	})
	if err != nil {
		return err
	}
	return nil
}

func addHandler() {
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/api/", apiHandler)
}

func main() {

	err := initDB()
	if err != nil {
		panic(err)
	}
	addHandler()
	log.Fatal(http.ListenAndServe(":80", nil))
}
