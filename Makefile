.PHONY: all go web

CWD := $(shell pwd)
make := $(shell which make)
docker := $(shell which docker)
mkdir := $(shell which mkdir)
rm := $(shell which rm)
echo := $(shell which echo)

all: ### Build all applications and libraries in right order.
	$(make) go
	$(make) web
	$(make) container

dependencies:
	$(mkdir) /tmp/build
	$(echo) -e "FROM golang:1.11.5-alpine\nRUN apk add --update gcc g++ git" > /tmp/build/Dockerfile
	$(docker) build -t gobuild /tmp/build
	$(rm) -rf /tmp/build

go: ## Build the it-glossary server application
	$(make) dependencies
	$(docker) run --rm -v $(CWD):/go/src/github.com/swinkelhofer/it-glossary -w /go/src/github.com/swinkelhofer/it-glossary gobuild sh -c "go get -v; CGO_ENABLED=1 GOOS=linux go build"

web: ### Build web dependencies
	$(docker) run --rm -v ${CWD}/static:/export -w /export node:10.14-alpine sh -c "npm install; npm run build"

container: ### Build Docker container
	$(docker) build -t it-glossary .

help: ### Prints this help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
