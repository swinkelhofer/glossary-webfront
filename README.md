# Glossary Webfront

![](screenshot.png)

## docker-compose.yml

```yaml
version: "2"

services:
  postgres:
    image: postgres:11.2-alpine
    environment:
      POSTGRES_PASSWORD: secret
      POSTGRES_DB: glossary
    volumes:
      - "./data:/var/lib/postgresql/data"
    restart: always
  web:
    image: registry.gitlab.com/swinkelhofer/glossary-webfront
    environment:
      POSTGRES_HOST: postgres
      POSTGRES_PASS: secret
      POSTGRES_DATABASE: glossary
    ports:
      - "80:80"
    restart: always
```

## Frameworks

 * Golang Webserver
 * go-pg Postgres ORM
 * Bulma
 * Vue.js
