FROM alpine:3.9

COPY static/dist/main.js /opt/static/dist/main.js
COPY static/de.svg /opt/static/de.svg
COPY it-glossary /opt/it-glossary 
COPY index.html /opt/index.html

WORKDIR /opt
EXPOSE 80

CMD ./it-glossary
