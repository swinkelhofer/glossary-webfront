'use strict'
const { VueLoaderPlugin } = require('vue-loader')
module.exports = {
    entry: [
        './src/app.js'
    ],
    module: {
        rules: [
        {
            test: /\.css$/,
            use : [
                'vue-style-loader',
                'css-loader'
            ],
        },
        {
            test: /\.scss$/,
            use: [
                'vue-style-loader',
                'css-loader',
                {
                    loader: 'sass-loader',
                },
            ],
        },
        {
            test   : /\.vue$/,
            loader : 'vue-loader',
        },
        {
            test   : /\.js$/,
            loader : 'babel-loader',
            exclude: /node_modules/
        },
        {
            test   : /\.(png|jpg|gif|svg|woff|ttf)$/,
            loader : 'file-loader',
            options: {
                name: '[name].[ext]?[hash]'
            }
        }
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.vue', '.json'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js' // Use the full build
        }
    },


    plugins: [
        new VueLoaderPlugin()
    ]
}

